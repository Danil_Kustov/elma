apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "elmabot-proxy.name" . }}
  labels:
    {{- include "elmabot-proxy.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "elmabot-proxy.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      annotations:
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        {{- include "elmabot-proxy.selectorLabels" . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/elmabot-proxy/service:{{ .Values.images.service }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: ELMA365_HOST
              value: {{ tpl ( .Values.global.host | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_HTTP_TIMEOUT
              value: {{ tpl ( .Values.appconfig.httpTimeout | quote ) . }}
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ tpl ( .Values.global.skipSslVerify | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_IDENTITY_SERVER_URL
              value: {{ tpl ( .Values.appconfig.identityServerUrl | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_API_URL
              value: {{ tpl ( .Values.appconfig.apiUrl | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_GRANT_TYPE
              value: {{ tpl ( .Values.appconfig.grantType | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_SCOPE
              value: {{ tpl ( .Values.appconfig.scope | quote ) . }}
            - name: ELMA365_ELMABOTPROXY_OIDC_URI
              value: {{ tpl ( .Values.appconfig.oidcUri | quote ) . }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: hydra-adaptor
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
