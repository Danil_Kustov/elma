apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "main.name" . }}
  labels:
    {{- include "main.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "main.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "main.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/main/gateway:{{ .Values.images.gateway }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: ELMA365_MAIN_STORE_URL
              value: {{ pluck .Values.global.env .Values.appconfig.store.url | first | default .Values.appconfig.store.url._default | quote }}
            - name: ELMA365_MAIN_WOPI_PRODUCTION
              value: {{ pluck .Values.global.env .Values.appconfig.wopi.productionEndpoints | first | default .Values.appconfig.wopi.productionEndpoints._default | quote }}
            {{- if .Values.global.wopi }}
            - name: ELMA365_MAIN_WOPI_EXTERNAL
              value: {{ .Values.global.wopi.externalEndpoint | quote }}
            - name: ELMA365_MAIN_WOPI_EXTERNAL_ENABLED
              value: {{ .Values.global.wopi.enabled | quote }}
            {{- if .Values.global.wopi.discovery }}
            - name: ELMA365_MAIN_WOPI_DISCOVERY
              value: {{ .Values.global.wopi.discovery | quote }}
            {{- end }}
            {{- end }}
            - name: ELMA365_NAMESPACE
              value: {{ default .Release.Namespace .Values.global.namespace }} # для редиса
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ .Values.global.skipSslVerify | quote }}
            - name: ELMA365_RABBITMQ_USER
              value: {{ template "main.rmquser" . }}
            - name: ELMA365_RABBITMQ_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: main
                  key: rabbitmq_password
            {{- if .Values.global.onpremiseVersion }}
            - name: ELMA365_ONPREMISE_VERSION
              value: {{ .Values.global.onpremiseVersion | quote}}
            {{- end }}
            {{- if .Values.global.onpremiseCommitHash }}
            - name: ELMA365_ONPREMISE_COMMIT_HASH
              value: {{ .Values.global.onpremiseCommitHash | quote }}
            {{- end }}
            - name: ELMA365_SETTINGS_SAVE_TEST_CODE
              value: {{ .Values.appconfig.settings.saveTestCode | quote }}
            - name: ELMA365_APP_MAX_WORKERS_COUNT
              value: {{ .Values.appconfig.app.maxWorkersCount | quote }}
            - name: ELMA365_APP_ITEMS_EXCHANGE_TIMEOUT
              value: {{ .Values.appconfig.app.itemsExchangeTimeout | quote }}
            - name: ELMA365_APP_EXCHANGE_TIMEOUT
              value: {{ .Values.appconfig.app.exchangeTimeout | quote }}
            - name: ELMA365_APP_ITEMS_EXCHANGE_IMPORT_CHUNK_SIZE
              value: {{ .Values.appconfig.app.itemsExchangeImportChunkSize | quote }}
            - name: ELMA365_LDAP_IMPORT_ATTEMPTS_MAX_COUNT
              value: {{ .Values.appconfig.ldap.attemptsMaxCount | quote }}
            - name: ELMA365_LDAP_IMPORT_DELAY_BETWEEN_ATTEMPTS
              value: {{ .Values.appconfig.ldap.delayBetweenAttempts | quote }}
            - name: ELMA365_LDAP_IMPORT_TIMEOUT
              value: {{ .Values.appconfig.ldap.importTimeout | quote }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
      {{- if eq .Values.global.solution "onPremise" }}
      {{- if .Values.global.ingress }}
      {{- if .Values.global.ingress.onpremiseTls }}
      {{- if .Values.global.ingress.onpremiseTls.enabledCA }}
          volumeMounts:
            - name: elma365-onpremise-ca
              subPath: elma365-onpremise-ca.pem
              mountPath: /etc/ssl/certs/elma365-onpremise-ca.pem
              readOnly: false
      volumes:
        - name: elma365-onpremise-ca
          configMap:
            name: {{ .Values.global.ingress.onpremiseTls.configCA }}
      {{- end }}
      {{- end }}
      {{- end }}
      {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "main.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
